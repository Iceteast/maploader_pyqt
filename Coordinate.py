#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

class Coordinate:

	def __init__(self, x, y, z):

		self.axisX = x
		self.axisY = y
		self.axisZ = z

	def getCoordinateXYZ(self):

		return self.axisX, self.axisY, self.axisZ

	def getCoordinateQR(self):

		return self.axisX, self.axisZ

