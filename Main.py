#! /usr/bin/env python3
#-*- coding:utf-8 -*-

#Author by Alex

import sys
import MainMap

from cfg import *
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QHBoxLayout, QVBoxLayout, \
    QDesktopWidget, QMessageBox, QWidget


class Frame(QMainWindow):
    #TODO 添加鼠标点击事件以及生成json等等。
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Map Editor ' + VERSION)
        self.resize(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.statbar = self.statusBar()
        self.statbar.showMessage('Ready')
        self.setCenter()

        widget = QWidget()
        self.setCentralWidget(widget)

        hbox = QHBoxLayout()
        #setMainMap
        self.mainMap = MainMap.Main(self)
        hbox.addWidget(self.mainMap)

        #setMenu
        menu = QVBoxLayout()

        btn_open = QPushButton("open", self)
        btn_open.clicked.connect(self.mainMap.openFile)

        btn_open = QPushButton("save", self)
        btn_open.clicked.connect(self.mainMap.saveFile)
        menu.addWidget(btn_open)

        hbox.addLayout(menu)

        widget.setLayout(hbox)
        self.show()

    #set Form im Center
    def setCenter(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, QCloseEvent):
        reply = QMessageBox.question(self,
                                     'Map Editor ' + VERSION,
                                     "Are you sure to exit ?",
                                     QMessageBox.No | QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Frame()
    sys.exit(app.exec_())
