#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

class Tile:

    def __init__(self, x, y, z):
        self.locate = (x, y, z)


class Entity:

    def __init__(self, coordinate, symbol):
        self.symbol = symbol
        self.coordinate = coordinate

    def getPosition(self):
        return self.coordinate

    def getSymbol(self):
        return self.symbol
