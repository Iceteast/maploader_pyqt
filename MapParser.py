#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

import json

import Coordinate
import cfg
from Element import Entity


class Map:

    def __init__(self, mapname='resources/test.json'):

        self.openFile(mapname)
        self.xMin = cfg.MAPRANGE[0][0]
        self.xMax = cfg.MAPRANGE[0][1]
        self.yMin = cfg.MAPRANGE[1][0]
        self.yMax = cfg.MAPRANGE[1][1]

    def openFile(self, filename):

        with open(filename) as k:
            self.jsdata = json.load(k)

        if self.jsdata == '':
            raise Exception("leeres Datei!!")

        self.rooms = self.jsdata['rooms']
        self.entities = self.jsdata['entities']

    def moveMap(self, direction):

        if direction == 'NORTH':
            self.xMin += 5
            self.xMax += 5
        elif direction == 'SOUTH':
            self.xMin -= 5
            self.xMax -= 5
        elif direction == 'EAST':
            self.yMin += 5
            self.yMax += 5
        elif direction == 'WEST':
            self.yMin -= 5
            self.yMax -= 5
        else:
            raise Exception("Wrong direction!")

    def getRange(self):

        return self.xMax - self.xMin, self.yMax - self.yMin

    def transQROnScreen(self, coordinate):

        return coordinate[0] + ((coordinate[1] + 1) // 2) - self.yMin, coordinate[1] - self.xMin

    def getNullPunkt(self):

        return self.transQROnScreen((0, 0))

    def getKeyPunkt(self):
        keyPunkt = []
        for i in range(-20, 20):
            for j in range(-20, 20):
                keyPunkt.append(self.transQROnScreen((i * 20, j * 20)))
        return keyPunkt

    def createRoomList(self):

        tileList = []
        for room in self.rooms:
            for tile in room['tiles']:
                tileList.append(
                    self.transQROnScreen(Coordinate.Coordinate(tile['x'], tile['y'], tile['z']).getCoordinateQR()))
        return tileList

    def getSymbol(self, entity):

        if entity['objectType'] == 'monster':
            return entity['monster']['type']
        elif entity['objectType'] == 'material':
            return entity['material']['type']
        elif entity['objectType'] == 'jewel':
            return entity['jewel']['type']
        else:
            return entity['objectType']

    def createEntityList(self):

        entityList = []

        for entity in self.entities:
            entityList.append(Entity(self.transQROnScreen(
                Coordinate.Coordinate(entity['position']['x'], entity['position']['y'],
                                      entity['position']['z']).getCoordinateQR()),
                cfg.OBJECTTYPE[self.getSymbol(entity)]))

        return entityList

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     ex = MapWindow(size=12)
#     sys.exit(app.exec_())
