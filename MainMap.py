#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

import math
import MapParser

from cfg import *
from PyQt5.QtGui import QPainterPath
from PyQt5.QtWidgets import QGraphicsTextItem, QGraphicsPathItem, QGraphicsScene, QGraphicsView


class Hex(QGraphicsPathItem):
    def __init__(self, parent, coordinate):
        super(Hex, self).__init__()

        self.statbar = parent.statbar
        self.coordinate = coordinate


class Main(QGraphicsView):

    def __init__(self, parent):
        super(Main, self).__init__(parent)

        # self.setMouseTracking(True)

        self.statbar = parent.statbar
        self.map = MapParser.Map(MAPPATH)
        self.size = INIT_HEX_SIZE
        self.tiledata = self.map.createRoomList()
        self.entitydata = self.map.createEntityList()
        self.np = self.map.getKeyPunkt()

        self.scene = QGraphicsScene()
        self.setDragMode(QGraphicsView.ScrollHandDrag)

        self.drawMap()
        self.setScene(self.scene)

    def createHex(self, coordinate):

        item = Hex(self, coordinate)

        if coordinate in self.tiledata:
            item.setBrush(MyColor.TILE.value)
        elif coordinate in self.np:
            item.setBrush(MyColor.KEYPOSITION.value)

        path = QPainterPath()

        sin = math.sin(math.radians(30)) * self.size
        cos = math.cos(math.radians(30)) * self.size
        (x, y) = self.qrToCoordinate(coordinate)

        path.moveTo(x, y + self.size)
        path.lineTo(x + cos, y + sin)
        path.lineTo(x + cos, y - sin)
        path.lineTo(x, y - self.size)
        path.lineTo(x - cos, y - sin)
        path.lineTo(x - cos, y + sin)
        path.lineTo(x, y + self.size)
        path.closeSubpath()

        item.setPath(path)

        return item

    def qrToCoordinate(self, coordinate):

        width = self.size * 2 * math.sin(math.radians(60))

        if (coordinate[1] % 2) != 0:
            return coordinate[0] * width, coordinate[1] * self.size * 1.53
        else:
            return coordinate[0] * width + width / 2, coordinate[1] * self.size * 1.53

    def createText(self, symbol, coordinate):

        x, y = self.qrToCoordinate(coordinate)

        text = QGraphicsTextItem()
        text.setFont(MyFont.SFONT.value)
        text.setPlainText(symbol)
        text.setDefaultTextColor(MyColor.SYMBOL.value)
        text.setPos(x - int(INIT_HEX_SIZE * 0.75), y - int(INIT_HEX_SIZE * 1.1))

        return text

    def createKeyPoint(self, coordinate):

        q, r = self.qrToCoordinate(coordinate)

        point_nw = QGraphicsTextItem()
        point_nw.setFont(MyFont.KFONT.value)
        point_nw.setPlainText(str(coordinate[0] + MAPRANGE[0][0]))
        point_nw.setDefaultTextColor(MyColor.COORDINATEX.value)
        point_nw.setPos(q - int(INIT_HEX_SIZE * 1.15), r - int(INIT_HEX_SIZE))

        point_s = QGraphicsTextItem()
        point_s.setFont(MyFont.KFONT.value)
        point_s.setPlainText(str(0 - (coordinate[0] + MAPRANGE[0][0]) - (coordinate[1] + MAPRANGE[1][0])))
        point_s.setDefaultTextColor(MyColor.COORDINATEY.value)
        point_s.setPos(q - int(INIT_HEX_SIZE * 0.41), r + int(INIT_HEX_SIZE * 0.2))

        point_ne = QGraphicsTextItem()
        point_ne.setFont(MyFont.KFONT.value)
        point_ne.setPlainText(str(coordinate[1] + MAPRANGE[1][0]))
        point_ne.setDefaultTextColor(MyColor.COORDINATEZ.value)
        point_ne.setPos(q + int(INIT_HEX_SIZE * 0.35), r - int(INIT_HEX_SIZE))

        self.scene.addItem(point_nw)
        self.scene.addItem(point_ne)
        self.scene.addItem(point_s)

    def drawMap(self):

        # draw Tiles
        for q in range(self.map.getRange()[0]):
            for r in range(self.map.getRange()[1]):
                self.scene.addItem(self.createHex((q, r)))
        # draw Entities
        for item in self.entitydata:
            self.scene.addItem(self.createText(item.getSymbol(), item.getPosition()))
        # draw key Coordinate
        for keyPunkt in self.np:
            self.createKeyPoint(keyPunkt)

    def openFile(self):

        self.map.openFile('resources/test.json')
        self.tiledata = self.map.createRoomList()
        self.entitydata = self.map.createEntityList()

        self.drawMap()

    def saveFile(self):

        self.map.openFile('resources/test.json')
        self.tiledata = self.map.createRoomList()
        self.entitydata = self.map.createEntityList()

        self.drawMap()
