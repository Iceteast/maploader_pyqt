#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

from enum import Enum
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QPen, QBrush, QFont

VERSION = 'v0.1'

SCREEN_WIDTH 	= 1024
MENU_WIDTH 		= 300
SCREEN_HEIGHT 	= 768
MAPRANGE 		= ((-100, 100), (-100, 100))
INIT_HEX_SIZE 	= 20
MAPPATH 		= 'resources/maps/testmap.json'

OBJECTTYPE = {"armor": '&',
              "chest": '=',
              "decoction": '¿',
              "door": 'D',
              "bug": 'å',
              "overflow": '¤',
              "tutor": '!',
              "assistant": 'μ',
              "professor": '¶',
              "player": '@',
              "potion": '?',
              "swordPart": '#',
              "weapon": '+',
              "wood": '|',
              "steel": 'æ',
              "leather": '~',
              "herbs": 'ÿ',
              "beetleshell": 'ø',
              "ruby": 'ò',
              "amethyst": 'ó',
              "emerald": 'ô',
              "diamond": 'õ',
              "table": 'n',
              "cauldron": 'u',
              "trunk": '×',
              "recipe": '®'}


class MyColor(Enum):
    BACKGROUND 	= QColor(0, 0, 0)
    TILE 		= QColor(112, 243, 255)
    KEYPOSITION = QColor(37, 37, 37)

    SYMBOL 		= QColor(177, 23, 23)

    COORDINATEX = QColor(100, 255, 77)
    COORDINATEY = QColor(255, 77, 100)
    COORDINATEZ = QColor(77, 100, 255)


class MyPen(Enum):
    BORDER = QPen(Qt.SolidLine)
    BORDER.setColor(QColor(175, 175, 75))
    BORDER.setWidth(2)

    CHOOSE = QPen(Qt.SolidLine)
    CHOOSE.setColor(QColor(75, 75, 175))
    CHOOSE.setWidth(3)


class MyBrush(Enum):
    TILE = QBrush(Qt.SolidPattern)
    TILE.setColor(QColor(112, 243, 255))


class MyFont(Enum):
    SFONT = QFont('Arial', int(INIT_HEX_SIZE * 0.9))
    KFONT = QFont('Arial', int(INIT_HEX_SIZE * 0.55))
